open Crawl

type block = < hash: string; level: int; predecessor: string > [@@deriving encoding {ignore}]
type header = < level: int; predecessor: string > [@@deriving encoding {ignore}]
type block_direct = < hash: string; header: header > [@@deriving encoding {ignore}]

let block_enc =
  let open Json_encoding in
  union [
    case block_enc Option.some Fun.id;
    case block_direct_enc (fun _ -> None)
      (fun b -> object
        method hash=b#hash
        method level=b#header#level
        method predecessor=b#header#predecessor
      end)
  ]

let head = ref None
let mem : (string, block * bool) Hashtbl.t = Hashtbl.create 1024

let set kind () b =
  let kind, main = match kind with `unset -> "unset", false | `register -> "register", true | `set -> "set", true in
  Format.printf "%s %s@." kind (EzEncoding.construct ~compact:true block_enc b);
  Hashtbl.add mem b#hash (b, main);
  if main then (match !head with
    | None -> head := Some b
    | Some h when h#level < b#level -> head := Some b
    | _ -> ());
  Lwt.return_ok ()

let get () s =
  Format.printf "get %s@." s;
  match Hashtbl.find_opt mem s with
  | None -> Lwt.return_ok `unknown
  | Some (b, true) -> Lwt.return_ok (`main b)
  | Some (b, false) -> Lwt.return_ok (`alt b)

let head () =
  Format.printf "head@.";
  Lwt.return_ok !head

let decode s = Lwt.return @@ EzEncoding.destruct_res block_enc s

let print_error = function
  | `http (code, reason) -> Format.eprintf "Http error %d: %s@." code reason
  | `exn exn -> Format.eprintf "Exception %s@." (Printexc.to_string exn)
  | `wrong_state -> Format.eprintf "Wrong state@."
  | `stopped -> Format.eprintf "Stopped@."
  | `destruct_exn exn -> Format.eprintf "Destruct exception %s@." (Printexc.to_string exn)

let stopr = ref false
let stop () = Lwt.return !stopr

let a_master () = { acc = (); set; get; head; stop; decode }

let%master [@field "bla"] a = a_master ()
let%plugin machin = {
  acc = 0;
  set = fun _kind acc _b -> Lwt.return_ok (acc + 1)
}

let () =
  let test ?(node="http://tz.functori.com") start =
    stopr := false;
    Lwt.map (Result.iter_error print_error) @@ Crawl.main ~node ~start a in
  Ezjs_min.export "test" @@
  object%js
    method test config =
      let node = Js_of_ocaml.Js.(Option.map to_string @@ Optdef.to_option config##.node) in
      test ?node config##.start
    method stop = stopr := true
  end
