open Crawl

let print_error = function
  | `http (code, reason) -> Format.eprintf "Http error %d: %s@." code reason
  | `exn exn -> Format.eprintf "Exception %s@." (Printexc.to_string exn)
  | `wrong_state -> Format.eprintf "Wrong state@."
  | `stopped -> Format.eprintf "Stopped@."
  | `destruct_exn exn -> Format.eprintf "Destruct exception %s@." (Printexc.to_string exn)
  | `pg_error s -> Format.eprintf "Pgocaml error %s@." s
  | `psql_error (name, fields) ->
    Format.eprintf "Psql error %s: %s@." name @@
    String.concat ", " @@ List.map (fun (c, s) -> (Format.sprintf "%C %s") c s) fields

let stopr = ref false
let stop _ = Lwt.return !stopr

let%master [@field "bla"] db = Pg.db ()
let%plugin machin = {
  acc = 0;
  set = fun _kind acc _b -> Lwt.return_ok (acc#machin + 1)
}

let () =
  stopr := false;
  let start = int_of_string Sys.argv.(1) in
  (try Tezos.node := Sys.argv.(2) with _ -> ());
  Lwt_main.run @@
  Lwt.map (Result.iter_error print_error) @@
  Crawl.main ~start {db; u=Tezos.util; chain=Tezos.chain; stop}
