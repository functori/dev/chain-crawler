val call : ?meth:Meth.t -> ?content:string -> ?content_type:string ->
  ?headers:(string * string) list -> string -> (string, [> `exn of exn | `http of (int * string)]) result Lwt.t

val stream : ?meth:Meth.t -> ?content:string -> ?content_type:string ->
  ?headers:(string * string) list -> url:string ->
  ('acc -> string -> [ `continue of 'acc | `stop of ('acc, [> `exn of exn | `http of (int * string)] as 'error) result] Lwt.t) ->
  'acc -> ('acc, 'error) result Lwt.t
