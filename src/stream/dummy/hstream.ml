let () =
  Format.eprintf
    "In your executable you need to load a particular implementaiton of `hstream`.\n\
     For unix based program, you can choose between chain-crawler.httpaf and chain-crawler.curl.\n\
     For js based program, you need to load chain-crawler.fetch@.";
  failwith "no hstream implementation loaded"

let call ?(meth:Meth.t option) ?(content:string option) ?(content_type:string option) ?(headers:(string * string) list option) (_: string) : (string, [> `exn of exn | `http of (int * string)]) result Lwt.t =
  ignore (meth, content, content_type, headers);
  assert false
let stream ?(meth:Meth.t option) ?(content:string option) ?(content_type:string option) ?(headers:(string * string) list option) ~(url:string) (_: 'acc -> string -> [ `continue of 'acc | `stop of ('acc, [> `exn of exn | `http of (int * string)] as 'error) result] Lwt.t) (_:'acc) : ('acc, 'error) result Lwt.t =
  ignore (meth, content, content_type, headers, url);
  assert false
