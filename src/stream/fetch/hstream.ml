open Ezjs_min_lwt
open Ezjs_fetch_lwt

let setup ?meth ?content ?content_type ?(headers=[]) () =
  let meth = Option.map Meth.to_string meth in
  let headers = (Option.fold ~none:[] ~some:(fun c -> ["content-type", c]) content_type) @ headers in
  let body = Option.map (fun s -> RString s) content in
  meth, headers, body

let call ?meth ?content ?content_type ?headers url =
  let meth, headers, body = setup ?meth ?content ?content_type ?headers () in
  Fun.flip Lwt.map (fetch ?meth ~headers ?body url to_text) @@ function
  | Error e -> Error (`exn (Failure Js_of_ocaml.Js_error.(to_string (of_error e))))
  | Ok r -> if r.ok then Ok r.body else Error (`http (r.status, r.body))

let stream ?meth ?content ?content_type ?headers ~url handler acc =
  let meth, headers, body = setup ?meth ?content ?content_type ?headers () in
  Lwt.bind (Promise.to_lwt @@ fetch_base ?meth ~headers ?body url) @@ function
  | Error e -> Lwt.return_error (`exn (Failure Js_of_ocaml.Js_error.(to_string (of_error e))))
  | Ok b ->
    let reader = Stream.get_reader b##.body in
    let rec aux acc =
      Lwt.bind (Promise.to_lwt reader##read) @@ function
      | Error e -> Lwt.return_error (`exn (Failure Js_of_ocaml.Js_error.(to_string (of_error e))))
      | Ok v ->
        match to_bool v##.done_, to_optdef Typed_array.String.of_uint8Array v##.value with
        | true, None -> Lwt.return_ok acc
        | true, Some v ->
          Fun.flip Lwt.map (handler acc v) (function
            | `continue a | `stop Ok a -> Ok a
            | `stop Error e -> Error e)
        | false, Some v ->
          Lwt.bind (handler acc v) (function
            | `continue a -> aux a
            | `stop x ->
              Lwt.bind (Promise.to_lwt @@ reader##cancel undefined) (fun _ -> Lwt.return x))
        | _ -> Lwt.return_error (`exn (Failure "undefined read result")) in
    aux acc
