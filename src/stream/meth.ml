type t = [
  | `CONNECT
  | `DELETE
  | `GET
  | `HEAD
  | `OPTIONS
  | `PATCH
  | `POST
  | `PUT
  | `TRACE
  | `Other of string
]

let to_string : t -> string = function
  | `CONNECT -> "CONNECT"
  | `DELETE -> "DELETE"
  | `GET -> "GET"
  | `HEAD -> "HEAD"
  | `OPTIONS -> "OPTIONS"
  | `PATCH -> "PATCH"
  | `POST -> "POST"
  | `PUT -> "PUT"
  | `TRACE -> "TRACE"
  | `Other s -> s
