
let setup ?meth ?content ?content_type ?(headers=[]) ?timeout ?verbose url =
  let headers =
    Option.fold ~none:[] ~some:(fun ct -> ["content-type: " ^ ct]) content_type @
    List.map (fun (name, value) -> Format.sprintf "%s: %s" name value) headers in
  let c = Curl.init () in
  Option.iter (Curl.set_timeout c) timeout;
  Option.iter (Curl.set_verbose c) verbose;
  Option.iter (fun m -> Curl.set_customrequest c @@ Meth.to_string m) meth;
  Curl.set_httpheader c headers;
  Curl.set_url c url;
  Option.iter (fun s -> Curl.set_postfields c s; Curl.set_postfieldsize c (String.length s)) content;
  c

let call ?meth ?content ?content_type ?headers url =
  let c = setup ?meth ?content ?content_type ?headers url in
  let b = Buffer.create 16384 in
  Curl.set_writefunction c (fun s -> Buffer.add_string b s; String.length s);
  try
    Curl.perform c;
    let rc = Curl.get_responsecode c in
    Curl.cleanup c;
    let data = Buffer.contents b in
    if rc >= 200 && rc < 300 then Lwt.return_ok data
    else
      let msg =
        if data <> "" then data
        else match Curl.curlCode_of_int rc with
          | Some c -> Curl.strerror c
          | None -> "" in
      Lwt.return_error (`http (rc, msg))
  with exn -> Lwt.return_error (`exn exn)

let wakeup_exn n c exn = Curl.cleanup c; Lwt.wakeup n (Error (`exn exn))
let async n c f = Fun.flip Lwt.dont_wait (wakeup_exn n c) f

let wfun n c cb acc = fun s ->
  async n c (fun () -> Fun.flip Lwt.map (cb !acc s) @@ function
    | `continue a -> acc := a
    | `stop x -> Curl.cleanup c; Lwt.wakeup n x);
  String.length s

let perform n c acc =
  async n c @@ fun () ->
  Fun.flip Lwt.map (Curl_lwt.perform c) @@ fun code ->
  Curl.cleanup c;
  let rc = Curl.int_of_curlCode code in
  let r =
    if rc >= 200 && rc < 300 then Ok !acc
    else Error (`http (rc, Curl.strerror code)) in
  Lwt.wakeup n r

let stream ?meth ?content ?content_type ?headers ~url cb acc =
  let w, n = Lwt.wait () in
  let c = setup ?meth ?content ?content_type ?headers url in
  let acc = ref acc in
  Curl.set_writefunction c (wfun n c cb acc);
  perform n c acc;
  w
