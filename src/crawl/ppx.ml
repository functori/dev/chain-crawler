open Ppxlib
open Ast_builder.Default

type plugin = {
  name: string;
  acc: expression;
  set: expression;
  def: expression -> expression;
  alone: bool;
}

let crawl_debug = match Sys.getenv_opt "CRAWL_DEBUG" with
  | Some "true" | Some "1" -> ref true
  | _ -> ref false

let extract_plugin ~name ~alone e =
  let loc = e.pexp_loc in
  let acc, set, def = match e.pexp_desc with
    | Pexp_record (l, _) ->
      let pl_acc = List.find_map (function ({txt=Lident "acc";_}, e) -> Some e | _ -> None) l in
      let pl_set = List.find_map (function ({txt=Lident "set";_}, e) -> Some e | _ -> None) l in
      begin match pl_acc, pl_set with
        | Some pl_acc, Some pl_set -> pl_acc, pl_set, Fun.id
        | _ -> Location.raise_errorf ~loc "missing fields in plugin record"
      end
    | Pexp_ident _ ->
      pexp_field ~loc e {loc; txt=Ldot (Lident "Crawl", "acc")},
      pexp_field ~loc e {loc; txt=Ldot (Lident "Crawl", "set")},
      Fun.id
    | _ ->
      pexp_field ~loc (evar ~loc ("_" ^ name)) {loc; txt=Ldot (Lident "Crawl", "acc")},
      pexp_field ~loc (evar ~loc ("_" ^ name)) {loc; txt=Ldot (Lident "Crawl", "set")},
      pexp_let ~loc Nonrecursive [ value_binding ~loc ~pat:(pvar ~loc ("_" ^ name)) ~expr:e ] in
  { name; acc; set; def; alone }

let extract_master ~name e =
  let loc = e.pexp_loc in
  let alone = true in
  match e.pexp_desc with
  | Pexp_record (l, _) ->
    let aux f = List.find_map (function ({txt=(Lident s| Ldot (Lident "Crawl", s)); _}, e) when s = f -> Some e | _ -> None) l in
    begin match aux "acc", aux "set", aux "get", aux "head" with
      | Some acc, Some set, Some pl_get, Some pl_head ->
        {name; acc; set; def=Fun.id; alone}, pl_get, pl_head
      | _ -> Location.raise_errorf ~loc "missing fields in master record"
    end
  | Pexp_ident _ ->
    let aux f = pexp_field ~loc e {loc; txt=Ldot (Lident "Crawl", f)} in
    {name; acc=aux "acc"; set=aux "set"; def=Fun.id; alone}, aux "get", aux "head"
  | _ ->
    let aux f = pexp_field ~loc (evar ~loc ("_" ^ name)) {loc; txt=Ldot (Lident "Crawl", f)} in
    {name; acc=aux "acc"; set=aux "set"; alone; def=pexp_let ~loc Nonrecursive [ value_binding ~loc ~pat:(pvar ~loc ("_" ^ name)) ~expr:e ]},
    aux "get", aux "head"

let process ~loc ?(name="config") ~plugins master =
  let pl_master, master_get, master_head = extract_master ~name master in
  let plugins = pl_master :: plugins in
  let fields_acc =
    List.map (fun pl ->
      pcf_method ~loc ({txt=pl.name; loc}, Public, Cfk_concrete (Fresh, pl.acc))) plugins in
  let acc = pexp_object ~loc @@ class_structure ~self:(ppat_any ~loc) ~fields:fields_acc in
  let rec set_plugins l e = match l with
    | [] -> e
    | pl :: tl ->
      let acc =
        if pl.alone then pexp_send ~loc [%expr _acc] {txt=pl.name; loc}
        else [%expr _acc] in
      [%expr
        Lwt.bind ([%e pl.set] _kind [%e acc] _block) @@ function
        | Error e -> Lwt.return_error e
        | Ok [%p pvar ~loc pl.name] -> [%e set_plugins tl e]] in
  let fields_set =
    List.map (fun pl -> pcf_method ~loc ({txt=pl.name; loc}, Public, Cfk_concrete (Fresh, evar ~loc pl.name))) plugins in
  let set = [%expr fun _kind _acc _block ->
    [%e set_plugins plugins @@ [%expr Lwt.return_ok [%e pexp_object ~loc @@ class_structure ~self:(ppat_any ~loc) ~fields:fields_set]]]] in
  let get = [%expr fun _acc -> [%e master_get] [%e pexp_send ~loc [%expr _acc] {loc; txt=name}]] in
  let head = [%expr fun _acc -> [%e master_head] [%e pexp_send ~loc [%expr _acc] {loc; txt=name}]] in
  List.fold_right (fun pl -> pl.def) plugins @@
  pexp_record ~loc [
    {loc; txt=Ldot (Lident "Crawl", "acc")}, acc;
    {loc; txt=Ldot (Lident "Crawl", "set")}, set;
    {loc; txt=Ldot (Lident "Crawl", "get")}, get;
    {loc; txt=Ldot (Lident "Crawl", "head")}, head;
  ] None

let ast_fold = object
  inherit [plugin list] Ast_traverse.fold
  method! structure_item it acc =
    match it.pstr_desc with
    | Pstr_extension (
      ({txt=("plugin"|"crawl.plugin"); _},
       PStr [ {pstr_desc=Pstr_value (_, [{pvb_expr; pvb_attributes; pvb_pat={ppat_desc=Ppat_var {txt; _}; _}; _}]); _} ]), _) ->
      let alone = List.exists (fun a -> a.attr_name.txt = "alone") pvb_attributes in
      let pl = extract_plugin ~name:txt ~alone pvb_expr in
      acc @ [ pl ]
    | _ -> acc
end

let ast_map = object(self)
  inherit Ast_traverse.map
  method! structure s =
    let plugins = ast_fold#structure s [] in
    Fun.flip List.filter_map s @@ fun it -> match it.pstr_desc with
    | Pstr_extension (({txt=("plugin"|"crawl.plugin"); _}, _), _) -> None
    | Pstr_extension (({txt=("master"|"crawl.master"); _}, PStr [ {pstr_desc=Pstr_value (_, [ vb ]); pstr_loc; _} ]), _) ->
      let name = List.find_map (fun a -> match a.attr_name.txt, a.attr_payload with
        | "field", PStr [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_constant Pconst_string (s, _, _); _}, _); _} ] -> Some s
        | _ -> None) vb.pvb_attributes in
      let expr = process ~loc:pstr_loc ?name ~plugins vb.pvb_expr in
      if !crawl_debug then Format.eprintf "%s@." (Pprintast.string_of_expression expr);
      Some (pstr_value ~loc:pstr_loc Nonrecursive [
        value_binding ~loc:pstr_loc ~pat:vb.pvb_pat ~expr
      ])
    | _ -> Some (self#structure_item it)
end

let () =
  Driver.register_transformation "crawl" ~impl:ast_map#structure
