let (let>) = Lwt.bind
let (let>?) p ok = Lwt.bind p (Result.fold ~ok ~error:Lwt.return_error)

type error = [ `wrong_state | `stopped ]
type kind = [ `register | `set | `unset ]

type ('block, 'acc, 'error) setter =
  kind -> 'acc -> 'block -> ('acc, 'error) result Lwt.t
  constraint 'error = [> error ]

type ('block, 'acc, 'error) plugin = {
  acc: 'acc;
  set: ('block, 'acc, 'error) setter;
} constraint 'error = [> error ]

type block_id = [`head | `hash of string | `level of int]

type 'block util = {
  hash: 'block -> string;
  level: 'block -> int;
  predecessor: 'block -> string;
}

type ('block, 'acc, 'error) listener = [
  | `one of ('acc -> ('block, 'error) result -> [`continue of 'acc | `stop of ('acc, 'error) result] Lwt.t) -> 'acc -> ('acc, 'error) result Lwt.t
  | `separate of ('acc -> (string, 'error) result -> [`continue of 'acc | `stop of ('acc, 'error) result] Lwt.t) -> 'acc -> ('acc, 'error) result Lwt.t
] constraint 'error = [> error ]

type ('block, 'acc, 'error) chain_block = {
  block: block_id -> ('block, 'error) result Lwt.t;
  listen: ('block, 'acc, 'error) listener;
} constraint 'error = [> error ]

type ('block, 'acc, 'error) db_block = {
  acc: 'acc;
  set: ('block, 'acc, 'error) setter;
  get: 'acc -> string -> ([`main of 'block | `alt of 'block | `unknown], 'error) result Lwt.t;
  head: 'acc -> ('block option, 'error) result Lwt.t;
} constraint 'error = [> error ]

type ('block, 'acc, 'error) action = {
  chain: ('block, 'acc, 'error) chain_block;
  db: ('block, 'acc, 'error) db_block;
  u: 'block util;
  stop: 'acc -> bool Lwt.t;
} constraint 'error = [> error ]

let forward ~start ~until a =
  let rec aux acc level =
    if level > until then Lwt.return_ok { a with db={a.db with acc} }
    else
      let> stop = a.stop acc in
      if stop then Lwt.return_error `stopped
      else
        let>? b = a.chain.block (`level level) in
        let>? acc = a.db.set `register acc b in
        aux acc (level+1) in
  aux a.db.acc start

let root a b =
  let rec aux bl b =
    let>? p = a.db.get a.db.acc (a.u.predecessor b) in
    match p with
    | `unknown ->
      let>? b = a.chain.block (`hash (a.u.predecessor b)) in
      aux (`unknown b :: bl) b
    | `alt b -> aux (`alt b :: bl) b
    | `main b -> Lwt.return_ok (b, bl) in
  let>? b2 = a.db.get a.db.acc (a.u.hash b) in
  match b2 with
  | `unknown -> aux [`unknown b] b
  | `alt _ -> aux [`alt b] b
  | `main _ -> Lwt.return_ok (b, [])

let reset a root b =
  let rec aux acc b =
    if a.u.hash b = a.u.hash root then Lwt.return_ok { a with db={a.db with acc} }
    else
      let>? acc = a.db.set `unset acc b in
      let>? p = a.db.get a.db.acc (a.u.predecessor b) in
      match p with
      | `main b -> aux acc b
      | _ -> Lwt.return_error `wrong_state in
  aux a.db.acc b

let update a l =
  let rec aux acc = function
    | `alt b :: tl ->
      let>? acc = a.db.set `set acc b in
      aux acc tl
    | `unknown b :: tl ->
      let>? acc = a.db.set `register acc b in
      aux acc tl
    | [] -> Lwt.return_ok {a with db={a.db with acc}} in
  aux a.db.acc l

let process a b =
  let>? h = a.db.head a.db.acc in
  match h with
  | None -> Lwt.return_error `wrong_state
  | Some h ->
    let>? r, bl = root a b in
    let>? a = reset a r h in
    update a bl

let check ~start a =
  let>? h = a.db.head a.db.acc in
  let aux () =
    let>? b = a.chain.block (`level start) in
    let>? acc = a.db.set `register a.db.acc b in
    Lwt.return_ok ({ a with db={a.db with acc}}, b, start+1) in
  match h with
  | None -> aux ()
  | Some h when a.u.level h < start -> aux ()
  | Some h ->
    let>? a = process a h in
    Lwt.return_ok (a, h, a.u.level h + 1)

let loop_aux_one ?head a =
  fun acc r ->
  let> stop = a.stop acc in
  if stop then Lwt.return @@ `stop (Ok acc)
  else
    match r, head with
    | Error e, _ -> Lwt.return @@ `stop (Error e)
    | Ok b, Some head when a.u.hash head = a.u.hash b -> Lwt.return @@ `continue acc
    | Ok b, _ ->
      let> r = process {a with db={a.db with acc}} b in
      match r with
      | Ok a -> Lwt.return @@ `continue a.db.acc
      | Error e -> Lwt.return @@ `stop (Error e)

let loop_aux_separate ?head a =
  fun acc r ->
  let> stop = a.stop acc in
  if stop then Lwt.return @@ `stop (Ok acc)
  else
    match r, head with
    | Error e, _ -> Lwt.return @@ `stop (Error e)
    | Ok hash, Some head when a.u.hash head = hash -> Lwt.return @@ `continue acc
    | Ok hash, _ ->
      let> r = a.chain.block (`hash hash) in
      match r with
      | Error e -> Lwt.return @@ `stop (Error e)
      | Ok b ->
        let> r = process {a with db={a.db with acc}} b in
        match r with
        | Ok a -> Lwt.return @@ `continue a.db.acc
        | Error e -> Lwt.return @@ `stop (Error e)

let loop ?head a =
  match a.chain.listen with
  | `one f -> f (loop_aux_one ?head a) a.db.acc
  | `separate f -> f (loop_aux_separate ?head a) a.db.acc

let main ~start a =
  let>? a, h_db, start = check ~start a in
  let>? h_chain = a.chain.block `head in
  let>? a = forward ~start ~until:(a.u.level h_chain) a in
  loop ~head:h_db a
