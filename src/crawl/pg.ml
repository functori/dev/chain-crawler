open Crawl

[%%pg {db="chain_indexer"; upgrade}]
[%%pg {
  version = 1;
  downgrade = [ "drop table blocks" ];
  upgrade = [ "create table blocks(hash varchar primary key, level int not null, predecessor varchar not null, main boolean not null)" ]
}]

let wrap f =
  try f () with
  | PGOCaml.Error s -> Lwt.return_error @@ `pg_error s
  | PGOCaml.PostgreSQL_Error (name, fields) -> Lwt.return_error @@ `psql_error (name, fields)

let set kind dbh b = wrap @@ fun () ->
  let () = match kind with
    | `register ->
      [%pgsql dbh
          "insert into blocks(hash, level, predecessor, main) \
           values(${b#hash}, ${Int32.of_int b#level}, ${b#predecessor}, true)"]
    | `set -> [%pgsql dbh "update blocks set main=true where hash = ${b#hash}"]
    | `unset -> [%pgsql dbh "update blocks set main=false where hash = ${b#hash}"] in
  Lwt.return_ok dbh

let get dbh s = wrap @@ fun () ->
  match [%pgsql.object dbh "select * from blocks where hash = $s"] with
  | [] -> Lwt.return_ok `unknown
  | h :: _ ->
    let b = object method hash = h#hash method predecessor = h#predecessor method level = Int32.to_int h#level end in
    if h#main then Lwt.return_ok @@ `main b
    else Lwt.return_ok @@ `alt b

let head dbh =
  match [%pgsql.object dbh "select * from blocks where main order by level desc limit 1"] with
  | [] -> Lwt.return_ok None
  | h :: _ ->
    let b = object method hash = h#hash method predecessor = h#predecessor method level = Int32.to_int h#level end in
    Lwt.return_ok (Some b)

let db () = { set; get; head; acc=PGOCaml.connect () }
