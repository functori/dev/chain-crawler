type block = < hash: string; level: int; predecessor: string > [@@deriving encoding {ignore}]
type header = < level: int; predecessor: string > [@@deriving encoding {ignore}]
type block_direct = < hash: string; header: header > [@@deriving encoding {ignore}]

let node = ref "http://tz.functori.com"

let block_enc =
  let open Json_encoding in
  union [
    case block_enc Option.some Fun.id;
    case block_direct_enc (fun _ -> None)
      (fun b -> object
        method hash=b#hash
        method level=b#header#level
        method predecessor=b#header#predecessor
      end)
  ]

let decode s = EzEncoding.destruct_res block_enc s

let block_url s = Format.sprintf "%s/chains/main/blocks/%s" !node s
let monitor_url () = Format.sprintf "%s/monitor/heads/main" !node

let hash b = b#hash
let predecessor b = b#predecessor
let level b = b#level

let block id =
  let id = match id with `head -> "head" | `hash h -> h | `level l -> string_of_int l in
  Lwt.bind (Hstream.call (block_url id)) @@ function
  | Error e -> Lwt.return_error e
  | Ok s -> Lwt.return (decode s)

let listen f acc =
  Hstream.stream ~url:(monitor_url ()) (fun acc s -> f acc (decode s)) acc

let util = { Crawl.hash; level; predecessor }
let chain = { Crawl.block; listen = `one listen }
